import { StyleSheet, View, Text } from "react-native";

export default function Home() {

  return (
    <View style={styles.container}>
      <Text>Home</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  button: {
    width: "80%",
    alignItems: "center",
    backgroundColor: "#0000FF",
    height: 40,
    justifyContent: "center",
    margin: 5,
    borderRadius: 50,
  },
  texto: {
    color: "#fff",
  },
});
