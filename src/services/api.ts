import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";

const config = {
  apiKey: "AIzaSyBdkgHtMo8LNYMj6ReU626BzHZJs3d2IGw",
  authDomain: "bazarapp-ffcff.firebaseapp.com",
  projectId: "bazarapp-ffcff",
  storageBucket: "bazarapp-ffcff.appspot.com",
  messagingSenderId: "951106098206",
  appId: "1:951106098206:web:1c65476be4d33e3580ca0f"
};

const app = initializeApp(config);
export const auth = getAuth(app);
